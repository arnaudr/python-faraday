Source: python-faraday
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-alembic,
               python3-apispec,
               python3-apispec-webframeworks,
               python3-autobahn,
               python3-bcrypt,
               python3-bleach,
               python3-faraday-agent-parameters-types,
               python3-faraday-plugins (>= 1.4.5),
               python3-filedepot,
               python3-filteralchemy,
               python3-flask,
               python3-flask-classful,
               python3-flask-kvsession (>= 0.6.3),
               python3-flask-limiter,
               python3-flask-principal,
               python3-flask-security,
               python3-flask-session,
               python3-flask-sqlalchemy (>=2.3.0),
               python3-flask-socketio,
               python3-hypothesis,
               python3-marshmallow (>= 3.0.0),
               python3-marshmallow-sqlalchemy (>= 0.26.0),
               python3-mock,
               python3-mockito (>= 1.2.2-2),
               python3-nplusone (>= 1.0.0-0kali4),
               python3-pil,
               python3-pyotp,
               python3-pytest,
               python3-pytest-factoryboy,
               python3-pytest-runner,
               python3-requests,
               python3-setuptools,
               python3-simplekv,
               python3-sphinx,
               python3-sqlalchemy,
               python3-syslog-rfc5424-formatter,
               python3-tqdm,
               python3-twisted,
               python3-webargs (>= 7.0.0),
Standards-Version: 4.5.1
Homepage: https://faradaysec.com
Vcs-Git: https://gitlab.com/kalilinux/packages/python-faraday.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/python-faraday

Package: faraday
Architecture: all
Depends: ${python3:Depends},
         ${sphinxdoc:Depends},
         curl,
         faraday-angular-frontend,
         gir1.2-gtk-3.0,
         gir1.2-vte-2.91,
         pgcli,
         postgresql,
         python3-distutils,
         python3-faraday-plugins (>= 1.4.5),
         python3-nplusone (>= 1.0.0-0kali4),
         python3-sqlalchemy-schemadisplay,
         python3-tornado,
         python3-webargs (>= 7.0.0),
         sudo,
         xdg-utils,
         zsh | zsh-beta,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: fonts-font-awesome,
            python3-bs4,
            python3-selenium,
            faraday-client,
            ruby | ruby-interpreter
Conflicts: python3-flask-babelex
Replaces: python-faraday (<< 3.10.0)
Breaks: python-faraday (<< 3.10.0)
Provides: python-faraday
Description: Collaborative Penetration Test IDE
 Faraday introduces a new concept (IPE) Integrated Penetration-Test Environment
 a multiuser Penetration test IDE. Designed for distribution, indexation and
 analysis of the generated data during the process of a security audit.
 .
 The main purpose of Faraday is to re-use the available tools in the community
 to take advantage of them in a multiuser way.
 .
 This package no longer contains the GTK client like the upstream repo.

Package: python-faraday
Architecture: all
Depends: faraday,
         ${misc:Depends}
Section: oldlibs
Description: Collaborative Penetration Test IDE
 Faraday introduces a new concept (IPE) Integrated Penetration-Test Environment
 a multiuser Penetration test IDE. Designed for distribution, indexation and
 analysis of the generated data during the process of a security audit.
 .
 The main purpose of Faraday is to re-use the available tools in the community
 to take advantage of them in a multiuser way.
 .
 This package is a transitional package. It can be remove safely.
